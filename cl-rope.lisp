;;; -*- Mode: LISP; Package: ROPE; outline-regexp:";;;;;*" -*-
;;;
;;; CL-ROPE -- a rope package for Common Lisp
;;; by Peter Gijsels, 2008
;;;
;;; An implementation of ropes (an alternative to strings) which supports
;;; efficient (O(log n)) :
;;;  - concatenation
;;;  - subrope selection
;;;  - element selection
;;;
;;;; References:
;;;  * Hans-J. Boehm, Russ Atkinson, Michiael Pass, 1995, "Ropes: an
;;;    Alternative to Strings"
;;;  * SGI's rope class implementation in C++ (an extension to the STL).
;;;
;;;  Limitations:
;;;  * There is currently no support for producing functions or lazily
;;;    copying of substrings.
;;;
;;;; Licence:
;;;
;;;  Permission is hereby granted, free of charge, to any person
;;;  obtaining a copy of this software and associated documentation files
;;;  (the "Software"), to deal in the Software without restriction,
;;;  including without limitation the rights to use, copy, modify, merge,
;;;  publish, distribute, sublicense, and/or sell copies of the Software,
;;;  and to permit persons to whom the Software is furnished to do so,
;;;  subject to the following conditions:
;;;
;;;  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;;;  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;;;  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;;;  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defpackage cl-rope
    (:use :cl)
    (:export
     #:advance
     #:at-end-p
     #:copy-rope-iterator
     #:current-elt
     #:iterator-position
     #:make-rope-iterator
     #:read-rope-from-stream
     #:rope
     #:rope-concatenate
     #:rope-length
     #:rope-elt
     #:sub-rope)))

(in-package cl-rope)

(declaim (optimize (speed 3) (debug 0)))

(defparameter *min-balanced-rope-lengths*
  (let ((fibs (list 2 1)))
    (loop for next-fib = (+ (first fibs) (second fibs))
       while (<= next-fib most-positive-fixnum)
       do (push next-fib fibs))
    (make-array (length fibs)
		:element-type 'fixnum
		:initial-contents (nreverse fibs)))
  "A balanced rope of depth i has a length of at least
   (aref *min-balanced-rope-lengths* i), the (+ i 2)'th Fibonacci number.")

(defparameter *max-rope-depth* (1- (length *min-balanced-rope-lengths*)))

(defparameter *max-print-length* 40
  "Ropes with a length larger than *max-print-length* will be abbreviated
  when printed.")

(defparameter *short-cut-off* 23
  "When concatenating leafs that are shorter than *short-cut-off*, we
  will allocate a leaf with a new string.")

(defparameter *initial-rope-chunk-size* 256
  "The initial length of the leaves when reading a rope from a stream.")

(declaim (type fixnum *short-cut-off* *initial-rope-chunk-size* *max-rope-depth*))

;;;; Ropes
(defstruct (rope (:constructor nil))
  (balanced-p nil) ; implies (>= length (aref *min-balanced-rope-lengths* depth))
  (depth 0 :type fixnum)
  (length 0 :type fixnum))

(defstruct (leaf (:include rope) (:constructor %make-leaf))
  (string "" :type (simple-array character (*))))

(defstruct (concat-node (:include rope) (:constructor %make-concat-node))
  left right)

;;;;; Construction
(declaim (inline %make-leaf make-leaf %make-concat-node))

(defun rope (x)
  "Make a rope for a string or character X."
  (make-leaf x))

(defun make-leaf (x)
  (etypecase x
    (string (%make-leaf :string x :length (length x) :balanced-p t))
    (character (%make-leaf :string (string x) :length 1 :balanced-p t))))

(defun make-concat-node (left right &optional (balanced-p nil))
  (%make-concat-node :depth (1+ (max (rope-depth left) (rope-depth right)))
		     :length (+ (rope-length left) (rope-length right))
		     :left left
		     :right right
		     :balanced-p balanced-p))

(defun read-rope-from-stream (stream)
  "Read a rope from STREAM."
  (let ((string (make-string *initial-rope-chunk-size*))
	(result (rope "")))
    (loop for chars-read = (read-sequence string stream)
       do (setf result (rope-concatenate result
					 (rope (subseq string 0 chars-read))))
	 while (= chars-read *initial-rope-chunk-size*))
    result))

;;;;; Sub ropes
(defun sub-rope (rope start &optional end)
  "Return a sub-rope of ROPE starting with element number START
and continuing to the end of the ROPE or the optional END."
  (let ((l (rope-length rope)))
    (unless (<= 0 start (or end l) l)
      (error "The bounding indices ~A and ~A are bad for a sequence of length ~A."
	     start
	     end
	     l))
    (labels
	((rec (rope start end)
	   (declare (type fixnum start end))
	   (declare (optimize (speed 3) (safety 0)))
	   (etypecase rope
	     (leaf (make-leaf (subseq (leaf-string rope) start end)))
	     (concat-node
	      (let* ((l (concat-node-left rope))
		     (r (concat-node-right rope))
		     (mid (rope-length l)))
		(declare (type fixnum mid))
		(cond
		  ((<= end mid)
		   (rec l start end))
		  ((<= mid start)
		   (rec r (- start mid) (- end mid)))
		  ((and (= 0 start) (= (rope-length rope) end))
		   rope)
		  (t
		   (make-concat-node
		    (rec-left l start)
		    (rec-right r (- end mid)))))))))
	 (rec-left (rope start)
	   "An optimized version of rec for the left subtree of a concat-node."
	   (declare (type fixnum start))
	   (declare (optimize (speed 3) (safety 0)))
	   (if (zerop start)
	       rope
	       (etypecase rope
		 (leaf (make-leaf (subseq (leaf-string rope) start)))
		 (concat-node
		  (let* ((l (concat-node-left rope))
			 (r (concat-node-right rope))
			 (mid (rope-length l)))
		    (declare (type fixnum mid))
		    (if (<= mid start)
			(rec-left r (- start mid))
			(make-concat-node
			 (rec-left l start)
			 r)))))))
	 (rec-right (rope end)
	   "An optimized version of rec for the right subtree of a concat-node."
	   (declare (type fixnum end))
	   (declare (optimize (speed 3) (safety 0)))
	   (if (= end (rope-length rope))
	       rope
	       (etypecase rope
		 (leaf (make-leaf (subseq (leaf-string rope) 0 end)))
		 (concat-node
		  (let* ((l (concat-node-left rope))
			 (mid (rope-length l)))
		    (declare (type fixnum mid))
		    (if (<= end mid)
			(rec-right l end)
			(make-concat-node
			 l
			 (rec-right (concat-node-right rope) (- end mid))))))))))
      (rec rope start (or end l)))))

;;;;; Concatenation (and balancing)
(declaim (inline short-leaf-p))
(defun short-leaf-p (rope)
  (and (leaf-p rope)
       (< (rope-length rope) *short-cut-off*)))

(declaim (inline concatenate-leaves))
(defun concatenate-leaves (leaf1 leaf2)
  (make-leaf (concatenate 'string (leaf-string leaf1) (leaf-string leaf2))))

(defun rope-concatenate (rope1 rope2)
  "Return a rope that is the concatenation of ROPE1 and ROPE2."
  (concat rope1 rope2))

(declaim (inline leaf-concat))
(defun leaf-concat (leaf1 leaf2)
  (make-leaf (concatenate 'string (leaf-string leaf1) (leaf-string leaf2))))

(defun tree-concat (left right)
  (let ((result (make-concat-node left right)))
    (if (and (> (rope-depth result) 20)
	     (or (< (rope-length result) 1000)
		 (> (rope-depth result) *max-rope-depth*)))
	(balance result)
	result)))

(defun concat (left right)
  (cond ((null left) right)
	((null right) left)
	((short-leaf-p right)
	 ;; Optimize the case of repeatedly appending one char.
	 (if (short-leaf-p left)
	     (leaf-concat left right)
	     (let ((l-r (concat-node-right left)))
	       (if (short-leaf-p l-r)
		   (make-concat-node (concat-node-left left)
				     (leaf-concat l-r right))
		   (tree-concat left right)))))
	(t (tree-concat left right))))

(defun concat-and-set-balanced (left right)
  (let ((result (concat left right)))
    (if (calculate-balanced result)
	(setf (rope-balanced-p result) t))
    result))

(defun calculate-balanced (rope)
  (>= (rope-length rope) (aref *min-balanced-rope-lengths* (rope-depth rope))))

(defun balance (r)
  (let ((forest (make-array (1+ *max-rope-depth*) :initial-element nil))
	(result nil))
    ;; (aref forest i) is one of the following:
    ;; - is nil
    ;; - is balanced and has a length in the interval [F(i+2),F(i+3))
    ;; The concatenation of the sequence of ropes in order of decreasing
    ;; length is equivalent to the prefix of R traversed so far.
    (add-to-forest r forest)
    (loop for y across forest
       if y
       do (setf result (concat y result)))
    result))

(defun add-to-forest (r forest)
  (if (rope-balanced-p r)
      (add-leaf-to-forest r forest)
      (progn
	(add-to-forest (concat-node-left r) forest)
	(add-to-forest (concat-node-right r) forest))))

(defun add-leaf-to-forest (r forest)
  (let* ((too-tiny nil)
	 (s (rope-length r))
	 (i (loop
	       for i from 0
	       while (>= s (aref *min-balanced-rope-lengths* (1+ i)))
	       for tree = (aref forest i)
	       if tree
	       do (progn
		    (setf too-tiny (concat-and-set-balanced tree too-tiny))
		    (setf (aref forest i) nil))
	       finally (return i)))
	 (insertee (concat-and-set-balanced too-tiny r)))
    (loop
       for i from i
       for tree = (aref forest i)
       if tree
       do (progn
	    (setf insertee (concat-and-set-balanced tree insertee))
	    (setf (aref forest i) nil))
       until (or (= i *max-rope-depth*)
		 (< (rope-length insertee) (aref *min-balanced-rope-lengths* (1+ i))))
       finally (setf (aref forest i) insertee))))

;;;;; Element selection
(defun rope-elt (rope index)
  (labels
      ((rec (rope index)
	 (declare (fixnum index))
	 (declare (optimize (speed 3) (safety 0)))
	 (etypecase rope
	   (leaf (elt (leaf-string rope) index))
	   (concat-node
	    (let* ((l (concat-node-left rope))
		   (mid (rope-length l)))
	      (if (< index mid)
		  (rec l index)
		  (rec (concat-node-right rope) (- index mid))))))))
    (rec rope index)))

;;;;; Utilities
(defun rope->string (rope)
  (let ((result (make-string (rope-length rope)))
	(pos 0))
    (loop
       for leaf in (leaves rope)
       for string = (leaf-string leaf)
       do (replace result string :start1 pos)
       do (incf pos (length string)))
    result))

(defmethod print-object ((x rope) stream)
  (let ((l (rope-length x)))
    (print-unreadable-object (x stream :type t)
      (format stream "(~A) " l)
      (if (< l *max-print-length*)
	  (format stream "~A" (rope->string x))
	  (format stream "~A..." (rope->string (sub-rope x 0 *max-print-length*)))))))

(defun leaves (rope)
  (if (leaf-p rope)
      (list rope)
      (nconc (leaves (concat-node-left rope))
	     (leaves (concat-node-right rope)))))

;;;;; Rope iterators
;; Only the leaf is stored, not the complete path to the leaf. This
;; saves some space and the code becomes simpler.
;; This might be suboptimal speedwise, but in practice it doesn't make
;; much difference.
(defstruct (rope-iterator (:constructor %make-rope-iterator (rope)))
  rope
  leaf
  (leaf-start 0 :type fixnum)
  (offset 0 :type fixnum))

(defun iterator-position (iterator)
  "Return the current position of ITERATOR (or length if at end)."
  (if (at-end-p iterator)
      (rope-length (rope-iterator-rope iterator))
      (+ (rope-iterator-leaf-start iterator)
	 (rope-iterator-offset iterator))))

(defun make-rope-iterator (rope &optional (i 0))
  (initialize-rope-iterator (%make-rope-iterator rope) i))

(defun initialize-rope-iterator (iterator i)
  (symbol-macrolet ((rope (rope-iterator-rope iterator))
		    (leaf (rope-iterator-leaf iterator))
		    (leaf-start (rope-iterator-leaf-start iterator))
		    (offset (rope-iterator-offset iterator)))
    (labels ((rec (rope s o)
	       (declare (fixnum s o))
	       (declare (optimize (speed 3) (safety 0)))
	       (etypecase rope
		 (leaf
		  (if (>= o (rope-length rope))
		      (setf leaf nil) ; at end
		      (progn
			(setf leaf rope)
			(setf leaf-start s)
			(setf offset o))))
		 (concat-node
		  (let* ((l (concat-node-left rope))
			 (mid (rope-length l)))
		    (if (< o mid)
			(rec l s o)
			(rec (concat-node-right rope) (+ s mid) (- o mid))))))))
      (rec rope 0 i))
    iterator))

(defun advance (iterator &optional (n 1))
  "Advance ITERATOR by N positions (N should be >= 0)."
  (declare (type fixnum n))
  (assert (not (minusp n)))
  (symbol-macrolet ((rope (rope-iterator-rope iterator))
		    (leaf (rope-iterator-leaf iterator))
		    (leaf-start (rope-iterator-leaf-start iterator))
		    (offset (rope-iterator-offset iterator)))
    (unless (at-end-p iterator)
      (let ((l (rope-length leaf)))
	(if (< (+ offset n) l)
	    (incf offset n) ; we're staying in the current leaf
	    (initialize-rope-iterator iterator (+ leaf-start offset n)))))
    iterator))

(defun current-elt (iterator)
  "Return the current element or nil if ITERATER is at-end-p."
  (if (at-end-p iterator)
      nil
      (rope-elt (rope-iterator-leaf iterator) (rope-iterator-offset iterator))))

(defun at-end-p (iterator)
  (null (rope-iterator-leaf iterator)))

;;;; Tests
#+5am(eval-when (:compile-toplevel :load-toplevel :execute)
       (use-package :fiveam))

#+5am
(progn
  (def-suite rope-test-suite)
  (in-suite rope-test-suite)

  ;; In the tests we're using sexps to represent the internal structure of
  ;; a rope. A sexp can be
  ;; - a string or character: corresponds to a leaf
  ;; - a list of exactly two elements: corresponds to a concat-node
  (defun sexp->rope (sexp)
    (if (or (characterp sexp) (stringp sexp))
	(rope sexp)
	(progn
	  (assert (= 2 (length sexp)))
	  (make-concat-node (sexp->rope (first sexp))
			    (sexp->rope (second sexp))))))

  (defun rope->sexp (rope)
    (etypecase rope
      (leaf (leaf-string rope))
      (concat-node (list (rope->sexp (concat-node-left rope))
			 (rope->sexp (concat-node-right rope))))))

  (defun mapply (fn list)
    (mapcar #'(lambda (x) (apply fn x)) list))

  (test min-rope-lengths-test
    (is (equalp
	 (loop for i below 7 collect (aref *min-balanced-rope-lengths* i))
	 '(1 2 3 5 8 13 21))))

  (test length-test
    (mapply #'(lambda (sexp expected-length)
		(is (= (rope-length (sexp->rope sexp)) expected-length)))
	    '(("" 0)
	      ("abc" 3)
	      (("foob" "ar") 6))))

  (test leaves-test
    (mapply #'(lambda (sexp expected)
		(equalp (leaves (sexp->rope sexp)) expected))
	    '(("" ())
	      ("abc" ("abc"))
	      ((("foo" "bar") "baz") ("foo" "bar" "baz")))))

  (test rope->string-test
    (mapply #'(lambda (sexp string)
		(is (string= (rope->string (sexp->rope sexp))
			     string)))
	    '(("" "")
	      (#\a "a")
	      ("abc" "abc")
	      (("foo" ("bar" "baz")) "foobarbaz"))))

  (test rope-concatenate-test
    (mapply #'(lambda (sexp1 sexp2 sexp3)
		(is (equalp (rope->sexp (rope-concatenate (sexp->rope sexp1)
							  (sexp->rope sexp2)))
			    sexp3)))
	    '(("" "" "")
	      ("abc" "def" "abcdef")
	      (("abc" "def") "g" ("abc" "defg"))
	      ("abc" ("def" "g") ("abc" ("def" "g")))
	      (("abc" "def") ("gh" "i") (("abc" "def") ("gh" "i"))))))
  
  (test sub-rope-test
    (mapply #'(lambda (sexp start end string)
		(is (string= (rope->string (sub-rope (sexp->rope sexp)
						     start end))
			     string)))
	    '(("abc" 0 0 "")
	      ("abc" 0 1 "a")
	      ("abc" 0 2 "ab")
	      ("abc" 0 3 "abc")
	      ("abc" 1 1 "")
	      ("abc" 1 2 "b")
	      ("abc" 1 3 "bc")
	      ("abc" 3 3 "")
	      (("ab" "cd") 0 0 "")
	      (("ab" "cd") 0 1 "a")
	      (("ab" "cd") 0 2 "ab")
	      (("ab" "cd") 0 3 "abc")
	      (("ab" "cd") 0 4 "abcd")
	      (("ab" "cd") 1 1 "")
	      (("ab" "cd") 1 2 "b")
	      (("ab" "cd") 1 3 "bc")
	      (("ab" "cd") 1 4 "bcd")
	      (("ab" "cd") 2 2 "")
	      (("ab" "cd") 2 3 "c")
	      (("ab" "cd") 2 4 "cd")
	      (("ab" "cd") 3 3 "")
	      (("ab" "cd") 3 4 "d")
	      (("ab" "cd") 4 4 "")))
    (let ((rope (sexp->rope '((("abc" "def") "ghi")
			      ("jkl" (("mno" "pqr")
				      ("stu" ("vw" "xyz"))))))))
      (loop for i below 26
	 do (setf rope (rope-concatenate (sub-rope rope 0 i)
					 (sub-rope rope i 26))))
      (is (string= (rope->string rope)
		   "abcdefghijklmnopqrstuvwxyz"))
      (loop for i below 26
	 do (setf rope (rope-concatenate (sub-rope rope i 26)
					 (sub-rope rope 0 i))))
      (is (= (rope-length rope) 26))))

  (test balance-test
    (let ((*short-cut-off* 1))
      (mapply #'(lambda (sexp expected)
		  (is (equalp (rope->sexp (balance (sexp->rope sexp)))
			      expected)))
	      '(("" "")
		("abc" "abc")
		(("abc" "de") ("abc" "de"))
		((("abc" "d") "e") ("abc" ("d" "e")))
		(((("a" ("b" "c")) "d") ("e" ("f" "g")))
		 ((("a" "b") ("c" "d")) (("e" "f") "g")))))))

  (test rope-elt-test
    (mapply #'(lambda (sexp index expected)
		(is (eql (rope-elt (sexp->rope sexp) index) expected)))
	    '((("ab" "cd") 0 #\a)
	      (("ab" "cd") 1 #\b)
	      (("ab" "cd") 2 #\c)
	      (("ab" "cd") 3 #\d))))

  (test rope-iterator-test
    (let* ((rope (sexp->rope '((("abc" "def") "ghi")
			       ("jkl" (("mno" "pqr")
				       ("stu" ("vw" "xyz")))))))
	   (it (make-rope-iterator rope)))
      (is (string= (coerce (loop
			      until (at-end-p it)
			      collect (current-elt it)
			      do (advance it))
			   'string)
		   (rope->string rope))))
    (let ((it (make-rope-iterator (sexp->rope '("a" ("bcd" "efg"))))))
      (is (zerop (iterator-position it)))
      (is (char= (current-elt it) #\a))
      (advance it 2)
      (is (= (iterator-position it) 2))
      (is (char= (current-elt it) #\c))
      (advance it 1)
      (is (= (iterator-position it) 3))
      (is (char= (current-elt it) #\d))
      (advance it 10)
      (is (at-end-p it))
      (is (= (iterator-position it) 7))
      (is (null (current-elt it))))))