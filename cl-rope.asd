;; -*- Mode: LISP -*-

(defpackage :cl-rope-system
  (:use :common-lisp :asdf))

(in-package :cl-rope-system)

(defsystem :cl-rope
  :author "Peter Gijsels <Peter.Gijsels+cl-rope@gmail.com>"
  :version "0.1"
  :components ((:file "cl-rope")))